<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class SitemapRecreateCommand extends ContainerAwareCommand
{
    /** @var Filesystem */
    private $fs;

    /** @var OutputInterface */
    private $output;

    private $sourceDir;

    private $targetDir;

    protected function configure()
    {
        $this
            ->setName('sitemap:recreate')
            ->setDescription('Make nice sitemaps')
            ->addArgument('source_dir', InputArgument::OPTIONAL, 'Folder with sitemaps')
            ->addArgument('target_dir', InputArgument::OPTIONAL, 'Folder for processed')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->fs = new Filesystem();
        $this->output = $output;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sourceDir = $input->getArgument('source_dir');
        $this->targetDir = $input->getArgument('target_dir');

        if (!$this->fs->exists($this->sourceDir)) {
            throw new \Exception(sprintf('FTP directory "%s" does not exist', $input->getArgument('source_dir')));
        }

        $this->directoryProcessing();

        return true;
    }

    private function directoryProcessing()
    {
        $finder = new Finder();
        $sourceFiles = $finder->files()->in($this->sourceDir)->depth(1);
        if (0 == count($sourceFiles)) {

            return true;
        }

        foreach ($sourceFiles as $sourceFile) {
            $fileExtension = $sourceFile->getExtension();

            switch ($fileExtension) {
                case 'htm':
                    $this->htmlProcessing($sourceFile);
                    break;
                case 'xml':
                    $this->xmlProcessing($sourceFile);
                    break;
                case 'txt':
                    $this->txtProcessing($sourceFile);
                    break;
            }
        }
    }

    private function htmlProcessing($sourceFile)
    {
        $content = file_get_contents($sourceFile->getPath() . '/' . $sourceFile->getFilename());

        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->loadHTML($content);

        $updatedDom = $this->fileProcessing($dom);

        $sourceFilePath = explode('/', $sourceFile->getPath());
        $locale = end($sourceFilePath);
        $toDir = $this->targetDir . '/seo/' . $locale;
        $this->saveToDisk($updatedDom, $toDir);

        $this->output->writeln("Complided: $toDir/sitemap.php");

        return true;
    }

    private function xmlProcessing($sourceFile)
    {
        $sourceFilePath = explode('/', $sourceFile->getPath());
        $locale = end($sourceFilePath);
        $toDir = $this->targetDir . '/seo/' . $locale;
        $this->fs->copy($sourceFile->getPath() . '/' . $sourceFile->getBasename(), $toDir . '/sitemap_xml.php');

        $this->output->writeln("Complided: $toDir/sitemap_xml.php");
    }

    private function txtProcessing($sourceFile)
    {
        $sourceFilePath = explode('/', $sourceFile->getPath());
        $locale = end($sourceFilePath);
        $toDir = $this->targetDir . '/seo/' . $locale;
        $this->fs->copy($sourceFile->getPath() . '/' . $sourceFile->getBasename(), $toDir . '/sitemap_links.php');

        $this->output->writeln("Complided: $toDir/sitemap_links.php");
    }

    private function fileProcessing(\DOMDocument $dom)
    {
        $dom = $this->headerProcessing($dom);
        $dom = $this->contentProcessing($dom);
        $dom = $this->footerProcessing($dom);
        $dom = $this->addCss($dom);

        return $dom;
    }

    private function headerProcessing(\DOMDocument $dom)
    {
        $xpath = new \DomXPath($dom);
        $a = $xpath->query("//div[@id='xsgHeader']//a")->item(0);
        if ($a instanceof \DOMElement) {
            $a->nodeValue = 'Diageo Bar Academy';
            $a->setAttribute('title', 'Diageo Bar Academy');
            $a->setAttribute('href', 'https://www.diageobaracademy.com');
        }

        return $dom;
    }

    private function contentProcessing(\DOMDocument $dom)
    {
        $dom = $this->uppercaseNodes($dom);
        $dom = $this->removeNeedLoginNodes($dom);

        return $dom;
    }

    private function uppercaseNodes(\DOMDocument $dom)
    {
        $xpath = new \DomXPath($dom);
        $links = $xpath->query("//div[@id='xsgBody']//a");

        foreach ($links as $link) {
            $link->nodeValue = htmlentities(strtoupper(html_entity_decode($link->nodeValue)));
        }

        return $dom;
    }

    private function removeNeedLoginNodes(\DOMDocument $dom)
    {
        $xpath = new \DomXPath($dom);
        $links = $xpath->query("//div[@id='xsgBody']//a");

        foreach ($links as $link) {
            if ('LOGIN' == $link->nodeValue) {
                $parentNode = $link->parentNode;
                if (('li' == $parentNode->tagName && $parentNode->getElementsByTagName('ul')->length === 0) || 'span' == $parentNode->tagName)  {
                    $link = $parentNode;
                    $parentNode = $parentNode->parentNode;
                }

                $parentNode->removeChild($link);

                if ('ul' == $parentNode->tagName &&
                    $parentNode->getElementsByTagName('li')->length === 0 &&
                    $parentNode->getElementsByTagName('ul')->length === 0) {
                    $parentNodeUp = $parentNode->parentNode;
                    $parentNodeUp->removeChild($parentNode);
                }
            }
        }

        return $dom;
    }

    private function footerProcessing(\DOMDocument $dom)
    {
        $footer = $dom->getElementById('xsgFooter');
        if ($footer) {
            $footer->parentNode->removeChild($footer);
        }

        return $dom;
    }

    private function addCss(\DOMDocument $dom)
    {
        $oldStyles = $dom->getElementsByTagName('style')->item(0);
        $css_text = trim($oldStyles->nodeValue);
        $css_text = str_replace(array("\n", "\r"), '', $css_text);

        $style = $dom->createElement('style', $css_text);
        $style->setAttribute('type', 'text/css');

        $head = $dom->getElementsByTagName('head')->item(0);
        $oldStyles->parentNode->removeChild($oldStyles);
        $head->appendChild($style);

        return $dom;
    }

    private function saveToDisk(\DOMDocument $updatedDom, $dirPath)
    {
        if (!$this->fs->exists($dirPath)) {
            $this->fs->mkdir($dirPath);
        }
        $newPath = $dirPath . '/sitemap.php';
        $updatedDom->save($newPath);
    }

}
